﻿using Robot.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Schema;

namespace Plazo.Pavlo.RobotChallange
{
    public class Plazo : IRobotAlgorithm
    {
        public string Author
        {
            get { return "Plazo Pavlo"; }
        }

        private int roundCount;

        public Plazo() =>
            Logger.OnLogRound +=
                (sender, args) =>
                    ++this.roundCount;

        public HashSet<int> myRobotsIndexes = new HashSet<int>();

        public EnergyStation FindNearestFreeStation(Robot.Common.Robot movingRobot, Map map,
            IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (IsStationFree(station, movingRobot, robots))
                {
                    int d = DistanceHelper.FindDistance(station.Position, movingRobot.Position);
                    if (d < minDistance)
                    {
                        minDistance = d;
                        nearest = station;
                    }
                }
            }

            return nearest == null ? null : nearest;
        }

        public bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot,
            IList<Robot.Common.Robot> robots)
        {
            return IsCellFree(station.Position, movingRobot, robots);
        }

        public bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot)
                {
                    if (robot.Position == cell)
                        return false;
                }
            }

            return true;
        }

        public Robot.Common.Robot FindProfitRobotToAttack(Robot.Common.Robot movingRobot, Map map,
            IList<Robot.Common.Robot> robots)
        {
            Robot.Common.Robot mostProfitRobotToAttack = new Robot.Common.Robot()
                {Energy = 0, Position = movingRobot.Position};
            foreach (var robot in robots)
            {

                if (((robot.Energy * 0.3) - DistanceHelper.FindDistance(robot.Position, movingRobot.Position)) >
                    ((mostProfitRobotToAttack.Energy * 0.3) -
                     DistanceHelper.FindDistance(mostProfitRobotToAttack.Position, movingRobot.Position)) &&
                    movingRobot.Energy > (DistanceHelper.FindDistance(robot.Position, movingRobot.Position))
                    && robot.OwnerName != "Plazo Pavlo")
                {
                    mostProfitRobotToAttack = robot;
                }
            }

            if (mostProfitRobotToAttack.Energy == 0)
                mostProfitRobotToAttack = null;

            return mostProfitRobotToAttack;
        }

        public EnergyStation FindProfitStationToMove(IList<Robot.Common.Robot> robots, Robot.Common.Robot movingRobot,
            Map map)
        {
            EnergyStation profitEnergyStationToMove = new EnergyStation()
                {Position = new Position(48, 48), Energy = 0, RecoveryRate = 2};

            foreach (var station in map.Stations)
            {
                if (IsStationFree(station, movingRobot, robots) && station.Energy -
                                                                DistanceHelper.FindDistance(station.Position,
                                                                    movingRobot.Position) >
                                                                profitEnergyStationToMove.Energy -
                                                                DistanceHelper.FindDistance(
                                                                    profitEnergyStationToMove.Position,
                                                                    movingRobot.Position)
                                                                && movingRobot.Energy >
                                                                DistanceHelper.FindDistance(station.Position,
                                                                    movingRobot.Position))
                {
                    profitEnergyStationToMove = station;
                }
            }

            if (profitEnergyStationToMove.Energy == 0)
                profitEnergyStationToMove = null;

            return profitEnergyStationToMove;
        }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            this.myRobotsIndexes.Add(robotToMoveIndex);
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            if ((movingRobot.Energy >= 2000)  && (myRobotsIndexes.Count < 100)) 
            {
                return new CreateNewRobotCommand() {NewRobotEnergy = movingRobot.Energy / 2};
            }

            EnergyStation positionOfNearStation = FindNearestFreeStation(robots[robotToMoveIndex], map, robots);

            if (positionOfNearStation == null)
            {
                Robot.Common.Robot mostProfitRobotToAttack =
                    FindProfitRobotToAttack(robots[robotToMoveIndex], map, robots);

                if (mostProfitRobotToAttack != null &&
                    (DistanceHelper.FindDistance(movingRobot.Position, mostProfitRobotToAttack.Position)) <
                    (mostProfitRobotToAttack.Energy * 0.3))
                {
                    return new MoveCommand() {NewPosition = mostProfitRobotToAttack.Position};
                }

                return null;
            }

            if (DistanceHelper.FindDistance(positionOfNearStation.Position, movingRobot.Position) <= 4)
            {
                Robot.Common.Robot mostProfitRobotToAttack =
                    FindProfitRobotToAttack(robots[robotToMoveIndex], map, robots);

                EnergyStation profitEnergyStationToMove =
                    FindProfitStationToMove(robots, robots[robotToMoveIndex], map);
                if (mostProfitRobotToAttack != null && profitEnergyStationToMove != null &&
                    positionOfNearStation.Energy <
                    ((mostProfitRobotToAttack.Energy * 0.3) -
                     DistanceHelper.FindDistance(mostProfitRobotToAttack.Position, movingRobot.Position))
                    && profitEnergyStationToMove.Energy - DistanceHelper.FindDistance(
                        profitEnergyStationToMove.Position,
                        movingRobot.Position) < ((mostProfitRobotToAttack.Energy * 0.3) -
                                                 DistanceHelper.FindDistance(mostProfitRobotToAttack.Position,
                                                     movingRobot.Position))
                    && DistanceHelper.FindDistance(mostProfitRobotToAttack.Position,
                        movingRobot.Position) < movingRobot.Energy)
                {
                    return new MoveCommand() {NewPosition = mostProfitRobotToAttack.Position};
                }
                else if (profitEnergyStationToMove != null && positionOfNearStation.Energy <
                                                           profitEnergyStationToMove.Energy -
                                                           DistanceHelper.FindDistance(
                                                               profitEnergyStationToMove.Position,
                                                               movingRobot.Position)
                                                           && DistanceHelper.FindDistance(
                                                               profitEnergyStationToMove.Position,
                                                               movingRobot.Position) < movingRobot.Energy)
                {
                    return new MoveCommand() {NewPosition = profitEnergyStationToMove.Position};
                }
                else return new CollectEnergyCommand();
            }

            else
            {
                EnergyStation profitEnergyStationToMove =
                    FindProfitStationToMove(robots, robots[robotToMoveIndex], map);
                if (profitEnergyStationToMove != null && positionOfNearStation.Energy <
                                                      profitEnergyStationToMove.Energy -
                                                      DistanceHelper.FindDistance(profitEnergyStationToMove.Position,
                                                          movingRobot.Position)
                                                      && DistanceHelper.FindDistance(profitEnergyStationToMove.Position,
                                                          movingRobot.Position) < movingRobot.Energy)
                {
                    return new MoveCommand() {NewPosition = profitEnergyStationToMove.Position};
                }

                Robot.Common.Robot mostprofitRobotToAttack =
                    FindProfitRobotToAttack(robots[robotToMoveIndex], map, robots);

                if ((mostprofitRobotToAttack != null) && positionOfNearStation.Energy <
                                                      ((mostprofitRobotToAttack.Energy * 0.3) -
                                                       DistanceHelper.FindDistance(mostprofitRobotToAttack.Position,
                                                           movingRobot.Position))
                                                      && DistanceHelper.FindDistance(mostprofitRobotToAttack.Position,
                                                          movingRobot.Position) < movingRobot.Energy)
                {
                    return new MoveCommand() {NewPosition = mostprofitRobotToAttack.Position};
                }
                else
                {
                    if (DistanceHelper.FindDistance(positionOfNearStation.Position,
                        movingRobot.Position) < movingRobot.Energy)
                        return new MoveCommand() {NewPosition = positionOfNearStation.Position};
                    else
                    {
                        Position pos = new Position();
                        pos.X = (movingRobot.Position.X + positionOfNearStation.Position.X) / 2;
                        pos.Y = (movingRobot.Position.Y + positionOfNearStation.Position.Y) / 2;
                        return new MoveCommand() {NewPosition = pos};
                    }
                }
            }
        }
    }
}